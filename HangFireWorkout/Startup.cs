using Hangfire;
using Hangfire.SqlServer;
using HangFireWorkout.Authorization;
using HangFireWorkout.Data.Entity;
using HangFireWorkout.Data.Models.BackgroundJob;
using HangFireWorkout.Extensions;
using HangFireWorkout.Service.Base;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Linq;

namespace HangFireWorkout
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHangfire(configuration => configuration
                                                  .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                                                  .UseSimpleAssemblyNameTypeSerializer()
                                                  .UseRecommendedSerializerSettings()
                                                  .UseSqlServerStorage(Configuration.GetConnectionString("SQLConnectionString"), 
                                 new SqlServerStorageOptions
                                 {
                                     CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
                                     SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
                                     QueuePollInterval = TimeSpan.Zero,
                                     UseRecommendedIsolationLevel = true,
                                     DisableGlobalLocks = true
                                 }));

            services.AddHangfireServer();

            services.AddSwaggerGen();
            services.AddControllers();


            RegisterServices(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IBackgroundJobClient jobClient, IServiceProvider serviceProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseHangfireDashboard("/hangfire", new DashboardOptions
            {
                Authorization = new[] { new HangfireAuthorizationFilter() }
            });

            app.UseHangfireServer();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHangfireDashboard();
            });
        }

        private void RegisterServices(IServiceCollection services)
        {
            services.AddDbContextPool<BaseContext>(options => options.UseSqlServer(Configuration["ConnectionStrings:SQLConnectionString"]));

            DependencyResolver resolver = new DependencyResolver();
            var servicesToResolve = resolver.FindClassesOfType(typeof(IService)).ToList();
            foreach (var service in servicesToResolve)
            {
                foreach (var serviceType in service.GetInterfaces())
                {
                    services.AddScoped(serviceType, service);
                }
            }


            var backgroundJobs = resolver.FindClassesOfType(typeof(IBackgroundJob)).ToList();
            foreach (var bgJob in backgroundJobs)
            {
                foreach (var bgJobType in bgJob.GetInterfaces())
                {
                    services.AddScoped(bgJobType, bgJob);
                }
            }

        }

    }
}
