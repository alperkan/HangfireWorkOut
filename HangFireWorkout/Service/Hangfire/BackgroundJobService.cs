﻿using Hangfire;
using HangFireWorkout.Data.Models.BackgroundJob;
using HangFireWorkout.Extensions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HangFireWorkout.Service.DemoJob
{
    public class BackgroundJobService : IBackgroundJobService
    {
        private readonly ILogger<BackgroundJobService> _logger;
        private readonly IServiceProvider _serviceProvider;
        private readonly IConfiguration _configuration;
        private readonly IBackgroundJobClient _backgroundJobClient;

        public BackgroundJobService(ILogger<BackgroundJobService> logger, IServiceProvider serviceProvider, IConfiguration configuration, IBackgroundJobClient backgroundJobClient)
        {
            _logger = logger ?? throw new Exception("Hangfire Job Service Logger instance could not be found!");
            _serviceProvider = serviceProvider ?? throw new Exception("Service Provider instance could not be found!");
            _configuration = configuration ?? throw new Exception("Configuration could not be found!");
            _backgroundJobClient = backgroundJobClient ?? throw new Exception("Background Job Client instance could not be found!");
        }

        public async Task Run()
        {
            //var jobConfig = _configuration.GetSection("BackgroundJobs:Jobs").Get<List<BackgroundJobConfigModel>>();

            var jobConfigs = new List<BackgroundJobConfigModel>();
            var jobConfigSection = _configuration.GetSection("BackgroundJobs:Jobs");
            foreach (IConfigurationSection section in jobConfigSection.GetChildren())
            {
                var bgJobConfig = new BackgroundJobConfigModel();
                bgJobConfig.JobType = section.GetValue<string>("JobType");
                bgJobConfig.CronParameters = section.GetValue<string>("CronParameters");
                bgJobConfig.JobId = section.GetValue<string>("JobId");
                jobConfigs.Add(bgJobConfig);
            }
            try
            {
                foreach(var bgJobConfig in jobConfigs)
                { 
                    await RegisterJob(bgJobConfig);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private async Task RegisterJob(BackgroundJobConfigModel config)
        {
            DependencyResolver resolver = new DependencyResolver();
            var jobType = resolver.FindClassesOfType(typeof(IBackgroundJob)).Single(q => q.Name == config.JobType);
            string name = "I"+jobType.Name;
            Type interfaceType = jobType.GetInterface(name);
            var service = (IBackgroundJob)_serviceProvider.GetService(interfaceType);
            await service.Run(config.JobId, config.CronParameters);
        }
    }
}
