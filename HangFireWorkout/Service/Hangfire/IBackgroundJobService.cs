﻿using HangFireWorkout.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HangFireWorkout.Service.DemoJob
{
    interface IBackgroundJobService: IService
    {
        Task Run();
    }
}
