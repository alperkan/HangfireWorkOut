﻿using System;
using System.Threading.Tasks;

namespace HangFireWorkout.Service.Demo
{
    public class DemoService: IDemoService
    {
        public DemoService()
        {

        }

        public async Task OutputToConsole()
        {
            Console.WriteLine("This is Demo Service");
        }
    }
}
