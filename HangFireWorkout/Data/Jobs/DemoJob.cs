﻿using Hangfire;
using HangFireWorkout.Service.Demo;
using System.Threading.Tasks;

namespace HangFireWorkout.Data.Jobs
{
    public class DemoJob : IDemoJob
    {

        public async Task Run(string jobId, string cronParameters)
        {
            RecurringJob.AddOrUpdate<IDemoService>(jobId, ds => ds.OutputToConsole(), cronParameters);
        }
    }
}
