﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HangFireWorkout.Data.Models.BackgroundJob
{
    public interface IBackgroundJob
    {
        Task Run(string jobId, string cronParameters);
    }
}
