﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HangFireWorkout.Data.Models.BackgroundJob
{
    public class BackgroundJobConfigModel
    {
        public string JobType { get; set; }
        public string JobId { get; set; }
        public string CronParameters { get; set; }
    }
}
