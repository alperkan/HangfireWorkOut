﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HangFireWorkout.Controllers
{
    public class BaseController: ControllerBase
    {
        protected readonly IConfiguration _configuration;
        protected readonly string _sqlConnectionString;

        public BaseController(IConfiguration configuration)
        {
            _configuration = configuration ?? throw new Exception("App Settings could not be read!");
            _sqlConnectionString = configuration.GetSection("ConnectionStrings")["SQLConnectionString"];
        }
    }
}
