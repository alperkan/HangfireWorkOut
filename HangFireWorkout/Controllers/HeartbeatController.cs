﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;

namespace HangFireWorkout.Controllers
{
    [ApiController]
    [Route("api/v1/heartbeat")]
    public class HeartbeatController : BaseController
    {
        private readonly ILogger<HeartbeatController> _logger;

        public HeartbeatController(IConfiguration configuration, ILogger<HeartbeatController> logger) : base(configuration)
        {
            _logger = logger ?? throw new Exception("Heartbeat Logger instance could not be found!");
        }

        [HttpGet("get-pulse")]
        public string GetPulse()
        {
            string connectionString = _sqlConnectionString;
            return "I'm fine!";
        }

       
    }
}
