﻿using HangFireWorkout.Service.DemoJob;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace HangFireWorkout.Controllers
{
    [ApiController]
    [Route("api/v1/background-job")]
    public class BackgroundJobController: BaseController
    {
        private readonly ILogger<BackgroundJobController> _logger;
        private readonly IServiceProvider _serviceProvider;
        
        public BackgroundJobController(IConfiguration configuration, IServiceProvider serviceProvider, ILogger<BackgroundJobController> logger) : base(configuration)
        {
            _logger = logger ?? throw new Exception("Background Job Logger instance could not be found!");
            _serviceProvider = serviceProvider ?? throw new Exception("ServiceProvider instance could not be found!");
        }

        [HttpGet("run-jobs")]
        public async Task RunJobs()
        {
            var service = (IBackgroundJobService)_serviceProvider.GetService(typeof(IBackgroundJobService));
            await service.Run();
        }

    }
}
