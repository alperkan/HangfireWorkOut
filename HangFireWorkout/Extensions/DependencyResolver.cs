﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace HangFireWorkout.Extensions
{
    public class DependencyResolver
    {
        private readonly bool _ignoreReflectionErrors = true;

        public string AssemblyRestrictToLoadingPattern { get; set; } = ".*";

        public string AssemblySkipLoadingPattern { get; set; } = "^System|^mscorlib|^Microsoft|^AjaxControlToolkit|^Antlr3|^Autofac|^AutoMapper|^Castle|^ComponentArt|^CppCodeProvider|^DotNetOpenAuth|^EntityFramework|^EPPlus|^FluentValidation|^ImageResizer|^itextsharp|^log4net|^MaxMind|^MbUnit|^MiniProfiler|^Mono.Math|^MvcContrib|^Newtonsoft|^NHibernate|^nunit|^Org.Mentalis|^PerlRegex|^QuickGraph|^Recaptcha|^Remotion|^RestSharp|^Rhino|^Telerik|^Iesi|^TestDriven|^TestFu|^UserAgentStringLibrary|^VJSharpCodeProvider|^WebActivator|^WebDev|^WebGrease";

        public bool LoadAppDomainAssemblies { get; set; } = true;

        public IList<string> AssemblyNames { get; } = new List<string>();

        public IEnumerable<Type> FindClassesOfType(Type assignTypeFrom)
        {
            IEnumerable<Assembly> assemblies = GetAssemblies();

            var result = new List<Type>();
            try
            {
                foreach (var a in assemblies)
                {
                    Type[] types = null;
                    try
                    {
                        types = a.GetTypes();
                    }
                    catch
                    {
                        //Entity Framework 6 doesn't allow getting types (throws an exception)
                        if (!_ignoreReflectionErrors)
                        {
                            throw;
                        }
                    }

                    if (types == null)
                        continue;

                    foreach (var t in types)
                    {
                        if (assignTypeFrom != null && !assignTypeFrom.IsAssignableFrom(t) && (!assignTypeFrom.IsGenericTypeDefinition || !DoesTypeImplementOpenGeneric(t, assignTypeFrom)))
                            continue;

                        if (t.IsInterface)
                            continue;

                        if (t.IsClass && !t.IsAbstract)
                        {
                            result.Add(t);
                        }

                    }
                }
            }
            catch (ReflectionTypeLoadException ex)
            {
                StringBuilder builder = new StringBuilder();

                foreach (var e in ex.LoaderExceptions)
                    builder.Append(e.Message + Environment.NewLine);

                var fail = new System.Exception(builder.ToString(), ex);
                Debug.WriteLine(fail.Message, fail);

                throw fail;
            }

            return result;
        }

        protected virtual bool DoesTypeImplementOpenGeneric(Type type, Type openGeneric)
        {
            try
            {
                if (openGeneric != null)
                {
                    var genericTypeDefinition = openGeneric.GetGenericTypeDefinition();
                    if (type != null)
                        foreach (var implementedInterface in type.FindInterfaces((objType, objCriteria) => true, null))
                        {
                            if (!implementedInterface.IsGenericType)
                                continue;

                            var isMatch =
                                genericTypeDefinition.IsAssignableFrom(implementedInterface.GetGenericTypeDefinition());
                            return isMatch;
                        }
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
        public virtual IList<Assembly> GetAssemblies()
        {
            var addedAssemblyNames = new List<string>();
            var assemblies = new List<Assembly>();

            if (LoadAppDomainAssemblies)
                AddAssembliesInAppDomain(addedAssemblyNames, assemblies);
            AddConfiguredAssemblies(addedAssemblyNames, assemblies);

            return assemblies;
        }

        protected virtual void AddConfiguredAssemblies(List<string> addedAssemblyNames, List<Assembly> assemblies)
        {
            foreach (var assemblyName in AssemblyNames)
            {
                var assembly = Assembly.Load(assemblyName);
                if (addedAssemblyNames != null && addedAssemblyNames.Contains(assembly.FullName))
                    continue;

                if (assemblies != null)
                {
                    assemblies.Add(assembly);
                }
                if (addedAssemblyNames != null)
                {
                    addedAssemblyNames.Add(assembly.FullName);
                }
            }
        }

        protected virtual bool Matches(string assemblyFullName, string pattern)
        {
            return Regex.IsMatch(assemblyFullName, pattern, RegexOptions.IgnoreCase | RegexOptions.Compiled);
        }

        public virtual bool Matches(string assemblyFullName)
        {
            return !Matches(assemblyFullName, AssemblySkipLoadingPattern)
                   && Matches(assemblyFullName, AssemblyRestrictToLoadingPattern);
        }

        private void AddAssembliesInAppDomain(List<string> addedAssemblyNames, List<Assembly> assemblies)
        {
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                if (!Matches(assembly.FullName))
                    continue;

                if (addedAssemblyNames.Contains(assembly.FullName))
                    continue;

                assemblies.Add(assembly);
                addedAssemblyNames.Add(assembly.FullName);
            }
        }
    }

}
